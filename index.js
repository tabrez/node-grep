// Search in multiple files, search for lines containing a string

const process = require('process');

if(process.argv.length < 4) {
  console.log('Usage: node index.js FILES word');
  console.log('Example: node index.js ./index.js Const');
  return;
}

const fs = require('fs');
const path = require('path');

const BUFFER_SIZE = 1024;
const word = process.argv[2];
const files = process.argv.slice(3, process.argv.length);

const search_word_in_files = function () {
  for(var i=0; i < files.length; ++i) {
    const file_path = path.normalize(files[i]);
    fs.exists(file_path, function(does_exist) {
      if(!does_exist) {
        console.error(file_path + ' file doesn\'t exist');
        return;
      }

      search_word_in_file(file_path);
    });
  }
}

search_word_in_files();

const search_word_in_file = function(file_path) {
  var buf = new Buffer(BUFFER_SIZE);

  fs.open(file_path, 'r', function(error, fd) {
    if(error) {
      console.error('error opening file: ' + file_path);
      return;
    }

    read_from_file(file_path, fd, buf);
  });
}

const read_from_file = function(file_path, fd, buf) {
  function loop() {
    fs.read(
      fd,
      buf,
      0,                        // state.buf offset
      buf.length,
      null,                     // file start position
      function(error, bytes) {
        if(error) {
          console.error('error reading content from ' + file_path);
          close_file(fd);
          return;
        }
        else if(bytes === 0) {
          close_file(fd);
          return;
        }

        const content = buf.slice(0, bytes).toString();
        // BUG: same line might be printed twice:
        // Unless there's new line character at the end of
        // block of text read, put the last element of the
        // array in 'partial' variable, prepend it to the
        // block of text read in next recursive call and
        // process it there
        const lines = content.split('\n');

        const results = lines.filter((line) => { return line.includes(word); })
        results.forEach((res) => { console.log(res); });

        loop();
      });
  }

  loop();
}

function close_file(fd) {
  fs.close(fd, function(error) {
    if(error)
      console.warn("error closing file: " + file_path);
  });
}
